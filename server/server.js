const base64 = require('base-64');

exports = {
    serverMethod: async function (options) {
        console.log("options ---->>>", options);
        var exotelAcc = options.iparams.Exotel_acc == "Singapore" ? 'api.exotel.com' : 'api.in.exotel.com';
        var exotelSID = options.iparams.Exotel_sid;
        let url = `https://${exotelAcc}/v2_beta/Accounts/${exotelSID}/IncomingPhoneNumbers`
        let Exotel_headers = {
            Authorization: `Basic ${base64.encode(`${options.iparams.exotel_api_key}` + ':' + `${options.iparams.exotel_api_token}`)}`,
            accept: "application/json",
        };
        try {
            var exophone = await exotelExophone(url, Exotel_headers);
            return renderData(null, exophone.response);
        } catch (error) {
            console.error('Cannot getexophone details = ', error);
            return renderData(error);
        }
    },

    OutboundCallMethod: async function (options) {

        var exotelAcc = options.iparams.Exotel_acc == "Singapore" ? 'api.exotel.com' : 'api.in.exotel.com';
        var exotelSID = options.iparams.Exotel_sid;
        var formData = options.formData;
        let url = `https://${exotelAcc}/v1/Accounts/${exotelSID}/Calls/connect.json`
        let Exotel_headers = {
            Authorization: `Basic ${base64.encode(`${options.iparams.exotel_api_key}` + ':' + `${options.iparams.exotel_api_token}`)}`,
            accept: "application/json",
        };
        try {
            var Outboundcalls = await Outboundcall(url, formData, Exotel_headers);
            return renderData(null, Outboundcalls.response);
        } catch (error) {
            console.error('Cannot Outboundcall details = ', error);
            return renderData(error);
        }
    }

}

async function exotelExophone(url, Exotel_headers) {
    return await $request.get(url, { headers: Exotel_headers });
}

async function Outboundcall(url, formData, Exotel_headers) {
    return await $request.post(url, { headers: Exotel_headers, formData });
}