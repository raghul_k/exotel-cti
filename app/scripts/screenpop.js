function unknownScreenPop(name, phone, direction, contactParam, callSid) {
    console.info('screenpop::unknownScreenPop')
    let contact = '<div class="incoming-call-sec sec-wdth bdr-rds10 min-hgt500" style="background: #0b2d46 !important;">' +
        '<div class="col-12">' +
        '<div class="caller-profile text-center" style="padding: 30px 110px 45px !important">' +
        '<div class="col-12">' +
        '<div class="row justify-content-md-center">' +
        '<div class="call-blink">' +
        '<div class="rounded-sec row align-items-center" style="width: 98px !important; height: 98px !important; background: #bebebe78 !important; border: 1px solid #ffffff7a !important;">' +
        '<span style="color: #ffffff !important;font-weight: 500 !important;">U</span>' +
        '<div class="wave" style="5px solid #757e8494 !important;">' +
        '</div>' +
        '<div class="wave1" style="border: 15px solid #757e8494 !important;">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="caller-details text-center">' +
        '<h4 class="name">' + name + '</h4>' +
        '<p class="number">' + phone + '</p>' +
        '</div>' + '<div class="call-action text-center">' +
        '<div class="row align-items-end min-hgt120"><br><br>' +
        '<div class="col-6">' +
        '<input type="hidden" value="unknown" id="caller">' +
        '<input type="hidden" value=' + direction + ' id="callerDirection">' +
        `<textarea style="display: none;" id="callMeta" rows="4" cols="50">
${JSON.stringify(contactParam)}
</textarea>`+
        `<textarea id="callSid" style="display: none;">${callSid}</textarea>` +
        '<button id="logcreate" style="background: #e5f2fd36 !important; padding: 0px 20px 0px 18px; border-radius: 10px; color: #ffffff !important;'+
        'font-size: 12px; font-weight: 600; border: 1px solid #e5f2fd36;margin: 0px -55px 40px 0px;height: 40px; width: 70% !important;"' +
        'data-num="' + { "name": "name1" } + '" onClick="newticketCreate(this)" class="pull-right btn call-atten w100">Add Note</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return contact;
}

function knownScreenPop(name, phone, direction, contactParam, callSid) {
    console.info('screenpop::knownScreenPop' + name, phone, direction, contactParam, callSid)
    let contact =
        '<div class="incoming-call-sec sec-wdth bdr-rds10 min-hgt500" style="background: #0b2d46 !important;">' +
        '<div class="col-12">' +
        '<div class="caller-profile text-center" style="padding: 30px 110px 45px !important">' +
        '<div class="col-12">' +
        '<div class="row justify-content-md-center">' +
        '<div class="call-blink">' +
        '<div class="rounded-sec row align-items-center" style="width: 98px !important; height: 98px !important; background: #bebebe78 !important; border: 1px solid #ffffff7a !important;">' +
        '<span style="color: #ffffff !important;font-weight: 500 !important;">' + name.substring(0, 1) + '</span>' +
        '<div class="wave" style="5px solid #757e8494 !important;">' +
        '</div>' +
        '<div class="wave1" style="border: 15px solid #757e8494 !important;">' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="caller-details text-center">' +
        '<h4 class="name">' + name + '</h4>' +
        '<p class="number">' + phone + '</p>' +
        '</div>' + '<div class="call-action text-center">' +
        '<div class="row align-items-end min-hgt120"><br><br>' +
        '<div class="col-6">' +
        '<input type="hidden" value="' + name + '" id="caller">' +
        '<input type="hidden" value="' + direction + '" id="callerDirection">' +
        `<textarea style="display: none;" id="callMeta" rows="4" cols="50">
${JSON.stringify(contactParam)}
</textarea>`+
        `<textarea id="callSid" style="display: none;">${callSid}</textarea>` +
        '<button style="background: #e5f2fd36 !important; padding: 0px 20px 0px 18px; border-radius: 10px; color: #ffffff !important;'+
        'font-size: 12px; font-weight: 600; border: 1px solid #e5f2fd36;margin: 0px -55px 40px 0px;height: 40px; width: 70% !important;"' +
        'data-num="' + { "name": "name1" } + '" onClick="newticketCreate(this)" class="pull-right btn call-atten w100">Add Note</button>' +
        '</div>' +
        '</div>' +
        '</div>';
    return contact;
}