function clickRecentTicket() {

    var callMeta = $('#callMeta').val();
    console.info('ticketHistory::callMeta : ', callMeta)
    if (JSON.parse(callMeta).contact[0] != undefined) {
        ticketHistoryShow(JSON.parse(callMeta).contact[0].id);
    }
    else {
        let noTicketsHistory = '<div class="card bg-primary text-white bg-info mb-3 cls_notkt" >' +
            '<div class="card-body text-center" style="font-size: 14px; font-weight: 600;">No Call log are Available</div>' +
            '</div><br>'
        $('#ticketss').empty()
        $('#ticketss').append(noTicketsHistory)
        $(".recentTickets").show()
    }
    $('#ticket-history').addClass("nav-tab-active active");
    $('#call-notes').removeClass("nav-tab-active active")
    $('#myTabContent').hide()
    $('#createTicketNotes').hide()

}

function ticketHistoryShow(requesterId) {
    console.info('ticketHistory::ticketHistoryShow::requesterId : ', requesterId)
    var headers = {
        "Authorization": 'Token token=' + fw_apikey,
        "Content-Type": "application/json"
    };

    var url = Clienturl + "/api/contacts/" + requesterId + "/notes?include=creater";
    console.log(url);
    client.request.get(url, { headers })
        .then((result) => addTicketsData(result),
            function (error) {
                if (error) {
                    console.error(error);
                }
            });
}

function addTicketsData(result) {

    var response = JSON.parse(result.response).notes;
    var res_response = response.reverse();
    console.log("*********call log response********************" + result.response);
    var ticketsHistory = '';
    if (res_response.length != 0) {
        ticketsHistory += `<div class="merge-ticket-sec sec-wdth bx-shadow bdr-rds10 bg-white">`
        for (let res of res_response) {
            var desc = res.description;
            if (desc.startsWith("https:")) {
                ticketsHistory +=
                    `<div class="merge-content-body" style="height: 90px !important;margin-top: 0px;">
                            <div class="col-12">
                            <div class="list-sec pl-xl-3 row align-items-center" style="border-radius: 12px !important;margin: 0px -5px 0px -15px;">
                            <div class="row align-items-center" style="width: 500px !important;">
                            <div class="col-2 pdrgt0" style="">
                            <div class="ticketIcon" onClick="ticketDeepLink('${res.id}')" data-toggle="tooltip" title="call log">
                            <button class="btn btn-round-white-hist" style="min-width: 34px !important; margin-top: -35px !important; height: 34px !important;">                         
                            <i class="fas fa-microphone" style="font-size: 16px;"></i>                            
                            </button>
                            </div>
                            </div> 
                            <div class="col-6 pdrgt0" style="">
                            <div class="desc">
                            <div style="display: flex !important;padding: 10px 5px 5px 5px !important;">
                            <audio controls style="height: 40px;">
                            <source src=${res.description} type="audio/mpeg">
                            Your browser does not support the audio element.
                            </audio>
                            </div>
                            <span class="w100" style="padding-top: 8px;padding-bottom: 10px;margin-left: 10px !important;padding-right: 20px;">${moment(res.updated_at).fromNow()}<p class="call_log_id">#${res.id}</p></span>                           
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>`
            } else {
                ticketsHistory +=
                    `<div class="merge-content-body" style="height: 90px !important;margin-top: 0px;">
                            <div class="col-12">
                            <div class="list-sec pl-xl-3 row align-items-center" style="border-radius: 12px !important;margin: 0px -5px 0px -15px;">
                            <div class="row align-items-center" style="width: 500px !important;">
                            <div class="col-2 pdrgt0" style="">
                            <div class="ticketIcon" onClick="ticketDeepLink('${res.id}')" data-toggle="tooltip" title="call log">
                            <button class="btn btn-round-white-hist" style="min-width: 34px !important; margin-top: -35px !important; height: 34px !important;">                          
                            <i class="fas fa-sticky-note" style="font-size: 16px;"></i>
                            </button>
                            </div>
                            </div> 
                            <div class="col-6 pdrgt0" style="">
                            <div class="desc">
                            <div style="display: flex !important;padding: 10px 0px 5px 10px!important;">
                            <p style="height: 40px;">${htmlEntities(res.description)}</p>
                            </div>
                            <span class="w100" style="padding-top: 5px;padding-bottom: 8px; margin-left: 10px !important;padding-right: 20px;">${moment(res.updated_at).fromNow()} <p class="call_log_id">#${res.id}</p></span>                           
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>`
            }
        }
    }
    else {
        ticketsHistory = '<div class="card bg-primary text-white bg-info mb-3 cls_notkt" >' +
            '<div class="card-body text-center" style="font-size: 14px; font-weight: 600;">No Call log are Available</div>' +
            '</div><br>'
    }
    $('#ticketss').empty();
    $('#ticketss').append(ticketsHistory);
    $(".recentTickets").show();
}

function htmlEntities(str) {
    return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}