$(document).ready(function () {
    app.initialized()
        .then(function (_client) {
            window.body = $('body');
            window.client = _client;
            client.instance.resize({ height: "500px" });
            addEventListeners();
            $(".mapListPage").hide();
            $(".ModalPopup").show();

            client.iparams.get().then((iparamValues) => {
                window.exotelSid = iparamValues.Exotel_sid;
                window.fw_Domain = `<%= iparam.fw_domain %>`;
                window.exotelDomain = iparamValues.Exotel_acc == "Singapore" ? '@api.exotel.com' : '@api.in.exotel.com';
                window.Endpoint_url = iparamValues.exotel_endpt_url;
                window.fw_apikey = `<%= iparam.fw_api_key %>`;
                window.exotel_apikey = iparamValues.exotel_api_key; //`<%= iparam.exotel_api_key %>`;
                window.exotel_apiToken = iparamValues.exotel_api_token; //`<%= iparam.exotel_api_token %>`;
                window.userMapping_access = iparamValues.userMapping_access;

                var Clientvalue = client.context.productContext.name;
                if (Clientvalue == "freshworks_crm") {
                    window.Clientname = "freshworksCrm";
                    window.Clienturl = client.context.productContext.url + "/sales";
                } else {
                    window.Clientname = client.context.productContext.name;
                    window.Clienturl = client.context.productContext.url;
                }

                console.log(exotelSid, fw_Domain, exotelDomain, Endpoint_url, fw_apikey, Clientname, Clienturl.split('://')[1]);
                appInitialize();
            }).catch(error => {
                if (error) console.error('Installation parameter error', error)
            })

        }).catch(function (err) {
            if (err) console.error('Main:App Initialized::error : ', err)
        })
});

function appInitialize() {
    client.data.get("loggedInUser").then(
        function (data) {
            console.info('Main:App Initialized::loggedInUser data : ', data)
            window.loggedInId = data.loggedInUser.id;
            window.loggedInName = data.loggedInUser.display_name;
            window.loggedInRoleIds = data.loggedInUser.role_id;
            window.loggedInNumber = data.loggedInUser.work_number;
            callHistory();
            socketConnection();
            getexophone();
            client.events.on("calling", eventCallback);
        }).catch(error => {
            if (error) console.error('Main:App Initialized::loggedInUser Error :', error)
        })
}

var eventCallback = function (event) {
    var toNumber = event.data.phoneNumber;
    var toContactId = event.data.targetableId;
    if (toNumber) {
        makeOutboundCall(toNumber);
    } else {
        let url_contact = Clienturl + `/api/contacts/${toContactId}`;
        var headers = {
            "Authorization": 'Token token=' + fw_apikey,
            "Content-Type": "application/json"
        }
        client.request.get(url_contact, { headers })
            .then(function (contact) {
                console.log("outbound call api, contact response for " + toContactId + " ::", jQuery.parseJSON(contact.response));
                contactNumber = jQuery.parseJSON(contact.response).contact.mobile_number;
                makeOutboundCall(contactNumber);
            }).catch((error) => {
                console.error('Cannot make Outgoing Call error = ', error)
                displayStatus('danger', 'Contact Not found')
            })
    }

};

function openCTIPanel(callInfo, apiValue, contactNumber) {
    client.interface.trigger("show", { id: "phoneApp" })
        .then(function (openCTI) {
            console.info('Interface OpenCTI : ' + openCTI)
            incomingScreenPop(callInfo, apiValue, contactNumber);
        }).catch(function (error) {
            if (error) console.error('Interface API show::openCTI error:', error)
        });
}

var exophoneNum = [];

function getexophone() {
    var options = {};
    client.request.invoke("serverMethod", options).then(
        function (data) {
            console.log("server method data is: " + JSON.stringify(data.response));
            console.log("exophone--------------->>>>>>>>>>>>>>>>>>>>>" + jQuery.parseJSON(data.response));
            let exoval = jQuery.parseJSON(data.response);
            if (exoval) {
                exoval.incoming_phone_numbers.forEach(element => {
                    var phone = element.phone_number;
                    var contactdata = phone.substr(phone.length - 10);
                    exophoneNum.push(contactdata)
                });
                console.log(exophoneNum);
            }
        },
        function (err) {
            console.error('Cannot getexophone details = ', err)
            displayStatus('danger', 'Cannot Get Exophone details. Please contact support for help')
        });
}

function addEventListeners() {
    client.events.on("cti.triggerDialer", clickToCallEvent)
}

function clickToCallEvent(event) {
    console.info('Main:clickToCallEvent::event : ' + event)
    makeOutboundCall(event.helper.getData().number)
};

function mapuserPhonedtl() {
    if (loggedInNumber) {
        return `<div class="col-8" style="padding : 0px;"><p class="cls_Pno"><i class="fas fa-phone-volume cls_mb"></i>${loggedInNumber}</p></div>`
    } else {
        return `<div class="col-8" style="padding : 0px;"><p class="cls_Pno"><i class="fas fa-phone-volume cls_mb"></i>${loggedInName}</p></div>`
    }
}

function getUserMappingIconHTML() {

    return `<div class="col-4"><button  class="fas fa-user-cog cls_setting" href="#" onClick="mappingUsers()" data-toggle="tooltip" data-placement="top" 
    title="User Mapping">
    </button></div>`
}



function getHeadingHTML() {

    return `

    </div>
    </div>  
    </div>
    <div class="history-calls">
    <div class="history-head">
        <div class="row">
    <div class="col-7 text-left">
        <h5 class="callHistory">Call history</h5>
    </div>
    <div class="col-5 text-right">
        <div class="all-calls">
    <select class="callTypeSelect" onChange="callHistory(this.value)">`;
}

function callHistory(...args) {
    console.info('Main:callHistory::args : ', args)
    let openingHTML = `<div class="sec-wdth bx-shadow bdr-rds10 bg-white min-hgt500">
    <div class="">
    <div class="row">
    <div class="col-12 text-left" style="display: flex">`
    let roles = loggedInRoleIds;
    console.info('Main:callHistory::Roles of logged in user : ', roles);

    var headers = {
        "Content-Type": "application/json"
    };
    var options = {
        headers: headers,
    };
    var url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
    client.request.get(url, options)
        .then((mappedNumber) => {
            let mappedNumberResponse = JSON.parse(mappedNumber.response);
            console.info('Main:callHistory::mappedNumber(/getMappedNumber API) : ', mappedNumberResponse);

            let logId = parseInt(loggedInId);
            let mappedNumberHTML = mapuserPhonedtl();
            let userMappingIconHTML = '';
            userMapping_access.forEach(usermapId => {
                if (loggedInId == usermapId) {
                    userMappingIconHTML = getUserMappingIconHTML();
                }
            });
            let headingHTML = getHeadingHTML();
            let getAllCallDetailsByAgentIdURL;
            let callTypeHTML = '';
            if (args.length == 0 || args[0] == 'allCalls') {
                getAllCallDetailsByAgentIdURL = `${Endpoint_url}/${exotelSid}/${logId}/calls`
                callTypeHTML = `<option value="allCalls" selected>All calls</option>
                                        <option value="missedCalls">Missed calls</option>`
            }
            else {
                getAllCallDetailsByAgentIdURL = `${Endpoint_url}/${exotelSid}/${logId}/calls?callState=missed`;
                callTypeHTML = `<option value="allCalls">All calls</option>
                                        <option value="missedCalls" selected>Missed calls</option>`
            }
            let closingHTML = `</select></div></div></div></div>`
            $('#callSid').val('')
            $('#callerDirection').val('')
            $('#callHistoryList').empty();
            $(".createTickets").hide();
            $(".callerOnCall").hide();
            $(".callerDetails").hide();
            $(".recentTickets").hide();
            $(".AllTickets").hide();

            var headers = {
                "Content-Type": "application/json"
            };
            var options = {
                headers: headers,
            };
            $('.ajax-loader').css("visibility", "visible");
            client.request.get(getAllCallDetailsByAgentIdURL, options)
                .then(
                    function (data) {
                        var result = jQuery.parseJSON(data.response);
                        if (result.error) {
                            $('.ajax-loader').css("visibility", "hidden");
                            let historyHtml = openingHTML + mappedNumberHTML + userMappingIconHTML + headingHTML + callTypeHTML + closingHTML;
                            historyHtml += `<div class="history-body"><div class="col-12">`;
                            historyHtml += `</div></div></div>` +
                                '<div class="card  mb-3" >' +
                                '<div class="card-body text-center">No Calls are Available</div>' +
                                '</div><br>'
                            $('#callHistoryList').append(
                                historyHtml
                            );
                        } else {
                            console.info("getAllCallDetailsByAgentIdURL API::result : ", result)
                            result.sort((a, b) => parseFloat(b.callLogId) - parseFloat(a.callLogId));
                            let historyHtml = openingHTML + mappedNumberHTML + userMappingIconHTML + headingHTML + callTypeHTML + closingHTML;
                            historyHtml += `<div class="history-body"><div class="col-12">`;
                            if (result.length != 0) {
                                historyHtml += callInformation(result);
                                $('.ajax-loader').css("visibility", "hidden");
                                historyHtml += `</div></div></div>`;
                                $('#callHistoryList').append(historyHtml);
                            } else {
                                $('.ajax-loader').css("visibility", "hidden");
                                historyHtml += `</div></div></div>` +
                                    '<div class="card  mb-3" >' +
                                    '<div class="card-body text-center">No Calls are Available</div>' +
                                    '</div><br>'
                                $('#callHistoryList').append(
                                    historyHtml
                                );
                            }
                        }

                    },
                    function (error) {
                        console.error('Error in showing Call history' + error)
                        displayStatus('danger', 'Refresh the Page');
                    });
            $('#callHistoryList').show();
        }).catch(function (err) {
            console.error('Main:callHistory::Roles error : ', err)
            displayStatus('danger', 'Error occured in Installation process. Please contact support for help')
        })
}

function getTickets(number, toNumber, callSid, direction, callState) {
    var contactnumber = "";
    if (direction == "outbound-api") {
        contactnumber = toNumber.substr(toNumber.length - 10);
    }
    else {
        contactnumber = number.substr(number.length - 10);
    }
    console.info('MAIN::getTickets::number: ' + contactnumber + 'callSid: ' + callSid + ' direction: ' + direction + 'toNumber:' + toNumber)
    console.info(`${Clienturl}/api/search?q= ` + encodeURIComponent(contactnumber) + `&include=contact`)
    client.request.get(Clienturl + "/api/search?q= " + encodeURIComponent(contactnumber) + "&include=contact", {
        headers: {
            "Authorization": 'Token token=' + fw_apikey,
            "Content-Type": "application/json"
        }
    })
        .then(function (contact) {
            var callDetails = {};
            console.info("Main::getTickets:Contact API response : ", jQuery.parseJSON(contact.response));
            if (contact.response == "[]") {
                getlead(contactnumber, callSid, direction, callState);
            } else {
                callDetails.contact = jQuery.parseJSON(contact.response);
                callDetails.phone = contactnumber;
                callDetails.direction = direction;
                if (jQuery.parseJSON(contact.response).length == 0) {
                    callDetails.phone = contactnumber
                    callDetails.name = 'Unknown'
                }
                $('#callHistoryList').append(`<textarea style="display: none;" id="callMeta" rows="4" cols="50">
                                                ${JSON.stringify(callDetails)}
                                            </textarea>` + `<textarea id="callCallSid" style="display: none;">${callSid}</textarea>
                                            <textarea id="callHistoryCallerDirection" style="display: none;">${direction}</textarea>
                                            <textarea id="callHistoryCallerState" style="display: none;">${callState}</textarea>`)
                newticketCreate(this);
            }
        },
            function (error) {
                console.error('Main:getTickets::GET Contact error', error);
                displayStatus('danger', 'Cannot get the Contact. Please contact support for help')
            })
}

function getlead(contactnumber, callSid, direction, callState) {
    client.request.get(Clienturl + "/api/search?q= " + encodeURIComponent(contactnumber) + "&include=lead", {
        headers: {
            "Authorization": 'Token token=' + fw_apikey,
            "Content-Type": "application/json"
        }
    }).then(function (lead) {
        var callDetails = {};
        console.info("Main::getTickets:lead API response : ", jQuery.parseJSON(lead.response));
        if (lead.response) {
            callDetails.contact = jQuery.parseJSON(lead.response);
            callDetails.phone = contactnumber;
            callDetails.direction = direction;
            if (jQuery.parseJSON(lead.response).length == 0) {
                callDetails.phone = contactnumber
                callDetails.name = 'Unknown'
            }
            $('#callHistoryList').append(`<textarea style="display: none;" id="callMeta" rows="4" cols="50">
                                            ${JSON.stringify(callDetails)}
                                        </textarea>` + `<textarea id="callCallSid" style="display: none;">${callSid}</textarea>
                                        <textarea id="callHistoryCallerDirection" style="display: none;">${direction}</textarea>
                                        <textarea id="callHistoryCallerState" style="display: none;">${callState}</textarea>`)
            newticketCreate(this);
        }
    },
        function (error) {
            console.error('Main:getTickets::GET Contact error', error);
            displayStatus('danger', 'Cannot get the lead. Please contact support for help')
        });
}

function mappingUsers() {
    client.interface.trigger("showModal", {
        title: "User Mapping",
        template: "mapping.html",
        data: { Endpoint_url: Endpoint_url, exotelSid: exotelSid, fw_Domain: fw_Domain, Clienturl: Clienturl, Clientname: Clientname, exotel_apikey: exotel_apikey, exotel_apiToken: exotel_apiToken, exotelDomain: exotelDomain, exophone_val: exophoneNum }
    }).then(function (data) {
        console.info("Main::mappingUsers::modal data : ", data);
    }).catch(function (error) {
        if (error) console.error('Error in User Mapping Window', error)
    });
}

function screenPop(pageRender) {
    $('.recentTickets').show();
    $(".Reply").hide();
    $(".ModalPopup").hide();
    $(".showContacts").hide();
    $('#callHistoryList').hide();
    $(".callerDetails").show();
    $(".callerDetails").empty();
    pageRender += footerHTML;
    $(".callerDetails").append(pageRender);
}