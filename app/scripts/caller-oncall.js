function getContactId(contact) {
   return contact == undefined ? 0 : contact.id;
}
var contactType = "";
var contactdirection = "";
var contactname = "";
function newticketCreate(contacphone) {
   console.info('caller-oncall::contacphone : ' + contacphone)
   var callMeta = $('#callMeta').val();
   console.info('caller-oncall::newticketCreate::callMeta : ', callMeta)
   let callMetaResponse = JSON.parse(callMeta);
   let contactId = getContactId(callMetaResponse.contact[0]);
   // var contactphone = callMetaResponse.contact[0] == undefined ? callMetaResponse.phone : callMetaResponse.contact[0].more_match.field_value;
   var contactphone = callMetaResponse.contact[0] == undefined ? callMetaResponse.phone : callMetaResponse.phone;
   contactname = callMetaResponse.contact[0] == undefined ? callMetaResponse.name : callMetaResponse.contact[0].name;
   contactType = callMetaResponse.contact[0] == undefined ? callMetaResponse.type : callMetaResponse.contact[0].type;
   contactdirection = callMetaResponse.direction == undefined ? callMetaResponse.direction : callMetaResponse.direction;
   console.info(`caller-oncall::newticketCreate::contactphone : ${contactphone} , contactname : ${contactname}`)
   $(".createTickets").hide();
   $(".callerOnCall").show();
   $(".callerDetails").hide();
   $(".recentTickets").hide();
   $("#AllTickets").hide();
   $('#callHistoryList').hide();
   let contactDeepLinkIcon = '';
   if (callMetaResponse.contact[0] != undefined && contactId !== 0) {
      contactDeepLinkIcon = `<div class="col-4 text-right" style="margin-left: 0px;">
                           <button class="cls_viewcon" id="nav-link active rounded-pro-sm" data-num="'+{"name":"name1"}+'" 
                           onClick="contactDeepLink('${callMetaResponse.contact[0].id}')" class="pull-right btn call-atten w100">
                           <i class="fas fa-eye" style="padding: 0px 5px 0px 0px; font-size: 11px;"></i>${contactType}</button>
                           </div>`
   }
   var replyHtmlObj = '';
   replyHtmlObj = `<div class="on-call-sec sec-wdth bx-shadow bdr-rds10 bg-white min-hgt500">
                  <div class="call-head pt-xl-2 pl-xl-3 pr-xl-3 pb-xl-2">
                  <div class="row align-items-center">
                  <div class="col-2 pdrgt0 cls_icon">
                  <div class="rounded-pro-sm">
                  <span>${contactname.substring(0, 1)}</span>
                  </div>
                  </div>
                  <div class="col-6 pl-xl-1 person-details">
                  <p>${contactname}</p>
                  <span>${contactphone}</span>	
                  </div>`
   replyHtmlObj += contactDeepLinkIcon;

   replyHtmlObj += ` </div>
                     <ul class="head-tabs-sec nav nav-tabs bg-white pt-xl-2 pl-xl-3 pr-xl-3 nav-tab-wrapper bg-dark-blue" id="myTab" role="tablist">
                     <li class="nav-item">
                     <a class="nav-link nav-tab nav-tab-active active" id="call-notes" data-toggle="tab" href="#" role="tab" aria-controls="home" aria-selected="true" onClick="clickCallNotes()">Call Notes</a>
                     </li>
                     <li class="nav-item">
                     <a class="nav-link nav-tab" id="ticket-history" data-toggle="tab" href="#" role="tab" aria-controls="home" aria-selected="true" onClick="clickRecentTicket()">Call log history</a>  
                     </li>
                     </ul>
                     </div>
                     <div class="content-body">
                     <div class="col-12 pad0">
                     <div style="padding-top: 0px; margin-top: 0px;" id="ticketss" class="recentTickets"></div>
                     <div>
                     <div class="tab-content active pt-xl-3 pl-xl-3 pr-xl-3 pb-xl-3" id="myTabContent">
                     <div class="tab-pane fade show tab-content active" id="tab-1" role="tabpanel" aria-labelledby="head-tabs">
                     <form class="call-notes-frm">`;

   if (contactId === 0) {
      replyHtmlObj += `<div class="form-group">
                     <label>Caller Name</label>
                     <input class="form-control contactname" type="text" placeholder="" name="callfrom"  id="contactname" style="font-size: 14px; font-weight: 600;">
                     <label>Caller Phone</label>
                     <input class="form-control contactemail" type="text" placeholder="" name="callfromemail" value=${contactphone}  id="contactemail" 
                     style="font-size: 14px; font-weight: 600; background-color: #f9f6f6 !important;" disabled>
                     </div>`;
   } else {
      replyHtmlObj += `<textarea id="contactname" style="display: none;">${contactname}</textarea>`;
   }

   if (callMetaResponse.contact[0] != undefined) {
      replyHtmlObj += `<textarea id="contactId" style="display: none;">${callMetaResponse.contact[0].id}</textarea>`;
   }

   replyHtmlObj += ` <div class="form-group">
                     <label>Call Notes</label>
                     <textarea class="form-control" rows="3" cols="50" placeholder="" name="callfrom"  id="description" style="height: 160px;"></textarea>
                     </div>
                     </form>
                     </div>
                     <div class="tab-pane fade tab-content" id="tab-2" role="tabpanel" aria-labelledby="head-tabs">
                     <form class="call-notes-frm">
                     <div class="form-group">
                     <label>Call Notes</label>
                     </div>
                     </form>
                     </div>
                     </div>
                     </div>
                     <div class="call-action-buttons pt-xl-4 pl-xl-4 pr-xl-4 pb-xl-4" id="createTicketNotes">
                     <div class="col-12">
                     <ul class="nav nav-pills pb-xl-3">
                     <li class="nav-item">
                     <button style="background-color:#1f3f58;color: #f9f9f9; min-width: 120px; border-radius: 7px; margin-left: 80px; font-weight: 400; font-size: 14px;" id="logcreate" data-num="'+{"name":"name1"}+'" onClick="createTicket('${contactphone}')" class="pull-right btn call-atten w100 ticketCreateButton">Add Call Log</button>
                     </li>
                     </ul>
                     </div>
                     </div>
                     </div>
                     </div>`;
   replyHtmlObj += footerHTML;
   $(".callerDetailsPage").empty();
   $(".callerDetailsPage").append(replyHtmlObj);
   $(".callerDetailsPage").show();
}

function htmlEntities(str) {
   return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
}

function createTicket(phoneNumber) {
   $(".ticketCreateButton").prop('disabled', true);
   var notes = $("#description").val();
   let callSid = getCallSid();
   var contactperson = "";
   if (contactname == "Unknown") {
      contactperson = $("#contactname").val();
   } else {
      contactperson = contactname;
   }
   console.info('caller-oncall::createTicket::callSid : ', callSid)
   let reqObject = {
      callSid: callSid,
      agentId: loggedInId,
      note: notes,
      name: contactperson,
      phone: phoneNumber,
      callDirection: contactdirection
   }
   console.log(reqObject);
   var headers = {
      "Content-Type": "application/json"
   };
   var options = {
      headers: headers,

      body: JSON.stringify(reqObject)
   };
   let getCallInfoEndPoint = `${Endpoint_url}/${Clientname}/${exotelSid}/createCallLog`
   console.log("calllog ur ------>" + getCallInfoEndPoint);

   client.request.post(getCallInfoEndPoint, options)
      .then((data) => {
         console.log("data------------->" + data.response);
         var calllogdata = JSON.parse(data.response);
         $('.ajax-loader').css("visibility", "visible");
         if (data.status == 200) {
            getNewlySavedContactDetails(phoneNumber);
            callHistory();
            displayStatus('success', 'Call Log #' + calllogdata.phone_calls[0].note_id + ' Created Successfully.');
         } else {
            displayStatus('danger', 'call Log Creation was Unsuccessfull.');
            $(".ticketCreateButton").prop('disabled', false);
         }
         $('.ajax-loader').css("visibility", "hidden");
      }).catch(error => {
         console.error('caller-oncall::createTicket::getCallsDetails::error: ', error)
         displayStatus('danger', 'Please contact support for help')
      })
}

function contactDeepLink(contactId) {
   console.info('caller-oncall::contactDeepLink::contactId : ', contactId)
   client.interface.trigger("show", { id: contactType, value: contactId })
      .then(function (data) {
         console.info('caller-oncall::contactDeepLink::response : ' + data)
      }).catch(function (error) {
         console.error('caller-oncall::contactDeepLink::Interface API Contact Click::error = ', error)
         displayStatus('danger', 'Please contact support for help')
      });
}

function clickCallNotes() {
   $('#call-notes').addClass("nav-tab-active active");
   $('#ticket-history').removeClass("nav-tab-active active")
   $('#myTabContent').show()
   $('#createTicketNotes').show()
   $(".recentTickets").hide()

}

function getCallSid() {
   let callSid = $('#callSid').val() == '' || $('#callSid').val() == undefined ? $('#callCallSid').val() : $('#callSid').val();
   console.info('caller-oncall::getCallSid::callSid : ', callSid)
   return callSid;
}

function getCallDirection() {
   let callDirection = $('#callerDirection').val() == '' || $('#callerDirection').val() == undefined ? $('#callHistoryCallerDirection').val() : $('#callerDirection').val();
   console.info('caller-oncall::getCallSid::callDirection : ', callDirection)
   return callDirection;
}

function getNewlySavedContactDetails(phone) {
   let currentValue = $('#callMeta').val();
   if (JSON.parse(currentValue).contact[0] == undefined) {
      client.request.get(Clienturl + "/api/search?q= " + encodeURIComponent(phone) + "&include=contact", {
         headers: {
            "Authorization": 'Token token=' + fw_apikey,
            "Content-Type": "application/json"
         }
      }).then((contact) => {
         var callDetails = {};
         callDetails.contact = jQuery.parseJSON(contact.response);
         $('#callMeta').val(JSON.stringify(callDetails));

      }, function (error) {
         console.error('caller-oncall:getNewlySavedContactDetails::GET Contact error : ' + error + ' ::phone : ' + phone);
         displayStatus('danger', 'Cannot get the Contact. Please contact support for help')
      })
   }
}

function getTicketSubject(callDirection, callState, phone) {

   if (callDirection == "incoming" && callState == "missed") {
      return `Missed call from ${phone}`;
   }
   else if (callDirection == "incoming") {
      return `Incoming call from ${phone}`;
   }
   else {
      return `Outbound call to ${phone}`;
   }

}