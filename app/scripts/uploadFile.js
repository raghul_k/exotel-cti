function fileUpload() {
    console.log("fileUpload entered");
    var updata = [];
    var success = [];
    var failed = [];
    Promise.all([client.db.get('uploadeddata'), client.db.get('successUpload'), client.db.get('FailureUpload')])
        .then(results => {

            updata.push(results[0]);
            success.push(results[1]);
            failed.push(results[2]);

            client.db.get('fileUpload').then(result => {
                console.info(`UploadFile::fileUpload::isUpload:${result.isUpload}`)
                if (result.isUpload) {
                    getLastUploadInfo(updata[0].updata, success[0].success, failed[0].failed, success[0].Date);
                }
                else {
                    document.getElementById('fileUploadInfo').innerHTML += `N/A`
                    document.getElementById('fileUploadInfo').style.display = 'block';
                }
            }).catch(err => {

                console.error(`UploadFile::fileUpload::Error in FDK DB Get:${JSON.stringify(err, null, 2)}`)
                displayStatus('danger', 'Error in DB process. Please Contact Support')

                document.getElementById('fileUploadInfo').innerHTML = `<h5 style="font-size: 16px;">Last Upload Status:</h5>N/A`
                document.getElementById('fileUploadInfo').style.display = 'block';
            })

            $('.fw-widget-wrapper').hide();
            $('.maplistpage').hide();
            $('#uploadPage').show();

            document.getElementById('fileUploadInfo').style.display = 'none';
            document.getElementById('uploadPageSubmitBtn').addEventListener('click', fileSelect);
        })
        .catch(error => {
            if (error) {
                console.log("error ---->>>", error);
            }
        });

    $('.fw-widget-wrapper').hide();
    $('.maplistpage').hide();
    $('#uploadPage').show();

    document.getElementById('fileUploadInfo').style.display = 'none';
    document.getElementById('uploadPageSubmitBtn').addEventListener('click', fileSelect);
}

function getLastUploadInfo(updata, success, failed, date) {
    document.getElementById('uploadPageSubmitBtn').disabled = false;

    document.getElementById('fileUploadInfo').innerHTML = `<h5 style="font-size: 16px;">Last Upload Status:</h5>`;
    let url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
    const headers = {
        "Content-Type": "application/json"
    };
    const options = {
        headers: headers,
    };
    client.request.get(url, options)
        .then((data) => {
            console.info(`UploadFile::getLastUploadInfo::UploadInfo:${data.response}`)
            var arr1 = JSON.parse(data.response);
            console.info(`UploadFile::getLastUploadInfo::\nUploadedAt:${date}\nTotalCount: ${arr1.length}\nEditedRecords: ${updata.length}\nSuccessCount:${success.length}\nFailureCount:${failed.length}`);
            if (data.response) {
                document.getElementById('fileUploadInfo').innerHTML += `File Uploaded At ${date} <br>Total Records: ${arr1.length}<br>Edited Records: ${updata.length}<br>Successful Records: ${success.length}<br>Failure Records: ${failed.length}<br><br>`
                document.getElementById('fileUploadInfo').style.display = 'block';
            }
            else {
                document.getElementById('fileUploadInfo').innerHTML += `N/A`
                document.getElementById('fileUploadInfo').style.display = 'block';
            }
        }).catch(error => {
            console.error(`UploadFile::getLastUploadInfo::Error in GET FileUpload API::Error:${JSON.stringify(error, null, 2)}`)
            displayStatus('danger', 'Error in getting last upload info. Contact Support')
        })

}

function fileSelect(evt) {

    console.info(`UploadFile::fileSelect::evt:${evt}`)
    var result = [];
    var finalResult = [];
    var file = $('#fileSelect')[0].files[0];
    console.log(file);
    if (!file) {
        console.error(`UploadFile::fileSelect::File Not found`)
        $('#fieldMapping').empty();
        $('#fieldMapping').addClass('errormessage')
        $('#fieldMapping').html('Choose file.');
        $('#fieldMapping').show();
        setTimeout(function () {
            $(`#fieldMapping`).hide();
            $('#fieldMapping').removeClass('errormessage')
        }, 3000);
    }
    else {
        $('.maplistpage').hide();
        document.getElementById('uploadPageSubmitBtn').disabled = true;

        // $('#fieldMapping').empty();
        // $('#fieldMapping').addClass('successmessage')
        // $('#fieldMapping').html('\tThe file upload will take some time.<br>Please revisit this section to see the upload status.');
        // $('#fieldMapping').show();

        var fileUpload = document.getElementById("fileSelect");
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var lines = e.target.result.split("\n");
                var real_header = ["agentId", "agentName", "exotelVirutalnumber", "exotelNumber", "isOutboundEnable"];
                for (var i = 1; i < lines.length; i++) {
                    var obj = {};
                    var currentline = lines[i].split(",");
                    obj["accountSid"] = exotelSid;
                    for (var j = 0; j < real_header.length; j++) {
                        var head = "";
                        var valuet = "";
                        var valuedata = "";
                        head = real_header[j];
                        if (head == "exotelNumber" || head == "exotelVirutalnumber") {
                            valuet = currentline[j];
                            //const count = valuet.substring(1, 0);
                            if (valuet) {
                                // valuedata = valuet.substring(valuet.length - 10);
                                valuedata = valuet;
                                obj[real_header[j]] = valuedata;
                            }
                            else {
                                obj[real_header[j]] = valuedata;
                            }
                        }
                        else {
                            obj[real_header[j]] = currentline[j];
                        }
                    }
                    obj["freshworksDomain"] = Clienturl.split('://')[1];
                    obj["type"] = Clientname;
                    result.push(obj);
                }
                result.splice(-1, 1);
                console.log(JSON.stringify(result));

                result.forEach(user => {
                    const numb = user.exotelNumber;
                    const str = numb.substring(1, 0);
                    if (str == "+") {
                        finalResult.push(user);
                    }
                });

                console.log("finalResult ::", JSON.stringify(finalResult));

                if (finalResult.length != result.length) {
                    $(`#fieldMapping`).hide();
                    $('#fieldMapping').empty();
                    $('#fieldMapping').addClass('errormessage')
                    $('#fieldMapping').html('Please add Country code wherever neccessary');
                    $('#fieldMapping').show();
                    setTimeout(function () {
                        $(`#fieldMapping`).hide();
                        $('#fieldMapping').removeClass('errormessage')
                    }, 5000);
                    document.getElementById('uploadPageSubmitBtn').disabled = false;
                } else {
                    $('#fieldMapping').empty();
                    $('#fieldMapping').addClass('successmessage')
                    $('#fieldMapping').html('\tThe file upload will take some time.<br>Please revisit this section to see the upload status.');
                    $('#fieldMapping').show();
                    setTimeout(function () {
                        $(`#fieldMapping`).hide();
                        $('#fieldMapping').removeClass('successmessage')
                    }, 5000);
                    document.getElementById('uploadPageSubmitBtn').disabled = false;
                }

                client.db.set('fileUpload', { "isUpload": true }).then(result => {
                    console.info(`UploadFile::fileSelect::DB SET data ${result}`)
                }).catch(err => {
                    console.error(`UploadFile::fileSelect::Error in FDK DB Set:${JSON.stringify(err, null, 2)}`)
                    displayStatus('danger', 'Error in DB process. Please Contact Support')
                    $(`#fieldMapping`).hide();
                    $('#fieldMapping').empty();
                    $('#fieldMapping').addClass('errormessage')
                    $('#fieldMapping').html('Please try again after sometime.');
                    $('#fieldMapping').show();
                    setTimeout(function () {
                        $(`#fieldMapping`).hide();
                        $('#fieldMapping').removeClass('errormessage')
                    }, 5000);
                    document.getElementById('uploadPageSubmitBtn').disabled = false;
                })

                var headers = {
                    "Content-Type": "application/json"
                };
                var options = {
                    headers: headers,
                    body: JSON.stringify(finalResult)
                };

                var url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
                client.request.post(url, options)
                    .then((data) => {

                        console.info(`UploadFile::fileSelect::Uploaded Result:${JSON.stringify(data)}`)
                        console.info(`UploadFile::fileSelect::Uploaded Result:${JSON.stringify(data.response)}`)

                        var updata = JSON.parse(data.response);
                        var success = [];
                        var failed = [];
                        updata.forEach(element => {
                            if (element.status == "success") {
                                success.push(element);
                            } else {
                                failed.push(element);
                            }
                        });

                        var now = new Date();
                        var date = now.toLocaleDateString();
                        var time = now.toLocaleTimeString();

                        var date_upload = "";
                        date_upload = date + ',' + time;


                        client.db.set('uploadeddata', { "updata": updata });
                        client.db.set('successUpload', { "success": success, "Date": date_upload });
                        client.db.set('FailureUpload', { "failed": failed });

                        getLastUploadInfo(updata, success, failed, date_upload);

                    },
                        function (err) {
                            console.error(`UploadFile::fileSelect::Error in DB data:${JSON.stringify(err)}`)
                            console.error(`UploadFile::fileSelect::Error in DB data::\nrequestID${err.requestID}\nstatus:${err.status}\nmessage:${err.message}`)
                            $(`#fieldMapping`).hide();
                            $('#fieldMapping').empty();
                            $('#fieldMapping').addClass('errormessage')
                            $('#fieldMapping').html('Please try again after sometime.');
                            $('#fieldMapping').show();
                            setTimeout(function () {
                                $(`#fieldMapping`).hide();
                                $('#fieldMapping').removeClass('errormessage')
                            }, 5000);
                            document.getElementById('uploadPageSubmitBtn').disabled = false;
                        });
            }
            reader.readAsText(fileUpload.files[0]);
        } else {
            console.log("This browser does not support HTML5.");
        }
    }

}

function downloadSampleCSV(...args) {

    console.info(`UploadFile::downloadSampleCSV::\nargs:${args}\nargs.length:${args.length}`)
    let url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
    if (args.length == 0) {
        document.getElementById("downloadBtn").disabled = true;
    }

    const headers = {
        "Content-Type": "application/json"
    };
    const options = {
        headers: headers,
    };
    client.request.get(url, options)
        .then((data) => {
            let userMappingList = JSON.parse(data.response);
            console.info('UploadFile::downloadSampleCSV::userMappingList: ', userMappingList)
            let csv = ''
            if (args.length == 0) {
                csv = 'Freshdesk Agent ID,Freshdesk Agent Name,CallerId (Exophone),Mobile Number,Click-2-Call (1 (yes) / 0 (No))\n'

                for (const userMapping of userMappingList) {
                    let exotelVirutalnumber = null;
                    let exotelNumber = '';
                    exotelVirutalnumber = userMapping.exotelVirutalnumber;
                    exotelNumber = userMapping.exotelNumber;
                    csv += `${userMapping.agentId},${userMapping.agentName},${exotelVirutalnumber},${exotelNumber},${userMapping.isOutboundEnable}\n`;
                }

                var hiddenElement = document.createElement('a');
                hiddenElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csv);
                hiddenElement.target = '_blank';
                hiddenElement.download = `${exotelSid}-sample.csv`;
                hiddenElement.click();
                document.getElementById("downloadBtn").disabled = false;
            }

        }).catch(error => {
            console.error('UploadFile::downloadSampleCSV::error in /usermapping/bulk: ', error)
            displayStatus('danger', 'Please contact support for help')
        })
}

function backAction() {
    $('#fileUploadInfo').hide()
    $('#fieldMapping').hide()
    $('#fileSelect').val('');
    $('.fw-widget-wrapper').show();
    $('.maplistpage').show();
    $('#uploadPage').hide();
    getMappingDetails(paginationPageNumber);
}