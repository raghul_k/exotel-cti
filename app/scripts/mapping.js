$(document).ready(function () {
    app.initialized()
        .then(function (_client) {
            window.body = $('body');
            window.client = _client;

            $('#newMappingForm').hide();
            $('.maplistpage').hide();
            $('#syncBtn').hide()
            $('#uploadBtn').hide()
            $('#chooseFilterLabel').hide()
            $('#chooseFilter').hide()
            $('#search').hide()
            $('#searchBtn').hide()
            $('#editMappingForm').hide();
            $(".newMappingForm").hide();

            client.instance.context().then(function (context) {

                console.info('Mapping::context data : ', JSON.stringify(context.data))
                window.Endpoint_url = context.data.Endpoint_url;
                window.exotelSid = context.data.exotelSid;
                window.fw_Domain = context.data.fw_Domain;
                window.Clientname = context.data.Clientname;
                window.Clienturl = context.data.Clienturl;
                window.Map_exotel_apikey = context.exotel_apikey; //`<%= iparam.exotel_api_key %>`;
                window.Map_exotel_apiToken = context.exotel_apiToken;
                window.exotelDomain = context.data.exotelDomain;
                window.exophoneNum = context.data.exophone_val;

                console.log("exophoneNum------------------>>>>>>>>>>>>>>>>" + exophoneNum);
                getMappingDetails();
            }).catch(function (error) {
                console.error('Instance API::error: ' + error)
                displayStatus('danger', 'Refresh the Page')
            });
            document.getElementById('uploadBtn').addEventListener('fwClick', fileUpload)
            document.getElementById("sync_yes_btn").addEventListener("fwClick", syncUserMapping);
            document.getElementById("sync_no_btn").addEventListener("fwClick", hideModal);

        },
            function (error) {
                if (error) console.error('Mapping::App initialized::error : ' + error);
            });
});

document.getElementById('searchBtn').addEventListener('fwClick', () => {

    let searchData = document.getElementById('search').value;
    console.info(`Mapping::searchData:${searchData}`)
    let filter = document.getElementById('chooseFilter').value;
    console.info('Mapping::searchData:filter', filter)
    let url = "";
    if (searchData) {
        url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMappingsearch?${filter}=${searchData}`
    }
    else {
        url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
    }

    var headers = {
        "Content-Type": "application/json"
    };
    var options = {
        headers: headers,
    };
    $('#spinner').css("visibility", "visible");
    getUsermapping(url, options, true);

})

function editinguser(editUserAgentId) {
    console.info("Mapping::editinguser::editUserAgentId : " + editUserAgentId);
    editMappingDetails();
}

function editMappingDetails() {
    var freshworksAgentId = $("#edit_freshdeskAgentId").val();
    var exotelNumber = $("#edit_exotelNumber").val();
    var exotelVirutalnumber = $("#edit_exotelVirutalnumber").val();
    var freshworksAgent = $("#edit_freshdeskAgentName").val();
    let isOutboundEnable = document.getElementById("enableOutboundEdit").checked ? 1 : 0;
    let freshworksdom = Clienturl.split('://')[1];
    let regEx = /^[+-]?\d+$/;
    if (freshworksAgentId != "" && exotelSid != "" && regEx.test(exotelVirutalnumber) && regEx.test(exotelNumber) && exotelNumber.length > 10 && exotelNumber.startsWith('+')) {
        let mappingObject = [{
            accountSid: exotelSid,
            agentId: freshworksAgentId,
            agentName: freshworksAgent,
            exotelNumber: exotelNumber,
            exotelVirutalnumber: exotelVirutalnumber,
            freshworksDomain: freshworksdom,
            isOutboundEnable: isOutboundEnable,
            type: Clientname
        }];

        console.info('Mapping::editMappingDetails::mappingObject : ', mappingObject);
        var headers = {
            "Content-Type": "application/json"
        };
        var options = {
            headers: headers,
            body: JSON.stringify(mappingObject)
        };
        var url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
        client.request.post(url, options)
            .then((data) => {
                console.info(`edited_response:::${JSON.stringify(data)}`)

                $('#editsuccessmessage p').html('Record has been edited successfully');
                $('#editsuccessmessage').show();
                setTimeout(function () {
                    $('#editsuccessmessage').hide();
                    $('#mappedUsers').empty();
                    getMappingDetails(paginationPageNumber);
                }, 3000);
            }).catch(error => {
                console.error('Mapping::editMap API::error: ' + JSON.stringify(error))

                if (JSON.parse(error.response).code === 2300) {
                    showErrorMessage('User already Exist.', 'editerrormessage')
                    displayStatus('danger', 'User already Exist.')
                }
                else if (JSON.parse(error.response).code === 2100) {
                    showErrorMessage(JSON.parse(error.response).description, 'editerrormessage')
                    displayStatus('danger', `${JSON.parse(error.response).message}`)
                }
                else {
                    showErrorMessage('Cannot edit the user mapping.', 'editerrormessage')
                    displayStatus('danger', 'Cannot edit the user mapping.')
                }
            })
    }
    else {
        if (!regEx.test(exotelVirutalnumber)) {
            showErrorMessage('Please Enter Valid Caller ID.', 'editerrormessage')
            displayStatus('danger', 'Please Enter Valid Caller ID.')
        }
        else if (!regEx.test(exotelNumber)) {
            showErrorMessage('Please Enter Valid Mobile Number.', 'editerrormessage')
            displayStatus('danger', 'Please Enter Valid Mobile Number.')
        }
        else if (!exotelNumber.startsWith('+')) {
            showErrorMessage('Please Enter Valid Country Code Starts with "+" on Mobile Number.', 'editerrormessage')
            displayStatus('danger', 'Please Enter Valid Country Code Starts with "+" on Mobile Number.')
        }
        else {
            showErrorMessage('Please Enter Valid details on required fields.', 'editerrormessage')
            displayStatus('danger', 'Please Enter Valid Valid details on required fields.')
        }
    }
}

function editbackAction() {
    $('#mappedUsers').empty();
    getMappingDetails(paginationPageNumber);
}

function getpaginationagents(mappedUserDetails) {
    let mappedUserHtml = ``;
    for (let mappedUserDetail of mappedUserDetails) {
        console.info('Mapping::getpaginationagents::isOutboundEnable : ', mappedUserDetail.isOutboundEnable)
        mappedUserHtml += `<tr>
                            </td>`;
        mappedUserHtml += (mappedUserDetail.exotelVirutalnumber && mappedUserDetail.uploadStatus != 'failure') ?
            `<td><span class="fdkagent" id="fdkagent">${mappedUserDetail.exotelVirutalnumber}</span> &ensp;</td>` :
            `<td><span class="fdkagent" id="fdkagent">${'-'}</span> &ensp;</td>`;
        mappedUserHtml += (mappedUserDetail.exotelNumber && mappedUserDetail.uploadStatus != 'failure') ?
            `<td><span>${mappedUserDetail.exotelNumber}</span></td>` :
            `<td><span>${'-'}</span></td>`;

        mappedUserHtml += `<td><span>${mappedUserDetail.agentName}(${mappedUserDetail.agentId})</span></td>
                            <td>`;

        if (mappedUserDetail.isOutboundEnable == 0) {
            mappedUserHtml += `<input type="checkbox" id="isOutboundEnable" name="isOutboundEnable" disabled>`
        }
        else {
            mappedUserHtml += `<input type="checkbox" id="isOutboundEnable" name="isOutboundEnable" checked disabled>`
        }

        mappedUserHtml += `</td><td class="agent">
                            <button data-agentId="${mappedUserDetail.agentId}"  data-accountSid="${mappedUserDetail.accountSid}" data-toggle="tooltip" data-placement="top" title="Edit" style="min-width:1%" class="btn edit btn-info btn-sm editmap cls_edit" id="edit"><i class="fas fa-edit" style="font-size: 18px; color: #6e6e6e;"></i></button>
                            </td>`
    }

    mappedUserHtml += `</tr>`;
    $('#mappedUsers').empty()
    $('#mappedUsers').append(mappedUserHtml);
    $('.maplistpage').show();
    $('#mappedUsers').show();
}

function getMappingDetails(...args) {
    console.info(`Mapping::getMappingDetails::args:${args}`)
    $('#fileUploadInfo').hide()

    $('#mappedUsers').empty()
    $('#mappedUsers').append(`<fw-spinner size="large" color="green"></fw-spinner>`);
    $('#mappedUsers').show();

    let start = 0;
    let end = 10;
    let pageNo = 1;
    let url;
    let storeCount = true;

    if (args.length !== 0) {
        pageNo = args[0];
        url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping?page=${pageNo}`
        storeCount = false;
        if (args[1]) {
            let chooseFilter = document.getElementById('chooseFilter').value;
            let searchData = document.getElementById('search').value;
            console.log(chooseFilter + ":" + searchData);
            url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMappingsearch=${encodeURIComponent(searchData)}&page=${pageNo}`
        }
    }
    else {
        url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping`;
        console.log(url);
    }
    var headers = {
        "Content-Type": "application/json"
    };
    var options = {
        headers: headers,
    };
    getUsermapping(url, options, storeCount, start, end);
}

function getUsermapping(url, options, storeCount, ...args) {

    let start = 0;
    let end = 10;

    if (args.length !== 0) {
        start = args[0];
        end = args[1];
    }

    client.request.get(url, options)
        .then(function (users) {
            $('#newMappingForm').hide()
            $('#editMappingForm').hide()
            $('#saveMapping').hide();

            $('#syncBtn').show();
            $('#uploadBtn').show();
            $('#chooseFilterLabel').show()
            $('#chooseFilter').show()
            $('#search').show();
            $('#searchBtn').show()
            $('.userMapPage').show();
            let mappedUserDetail = JSON.parse(users.response);
            console.info("Mapping::getMappingDetails::mappedUserDetail : ", mappedUserDetail);

            if (storeCount) {
                let mappedUserCount = parseInt(mappedUserDetail.length);
                let pageCount = Math.ceil(parseInt(mappedUserCount) / 10);
                console.info('Mapping::getMappingDetails::pageCount : ', pageCount);

                let paginationHTML = `<select id="selectPageNo" style="width: 40px;" onChange="selectPageNo()">`
                for (let index = 1; index <= pageCount; index++) {
                    paginationHTML += index === 1 ? `<option selected="selected" value="1">1</option>` : `<option value="${index}">${index}</option>`;
                }
                paginationHTML += `</select><p style="margin-left: 50px; margin-top: -23px;">of ${pageCount} pages<p>`
                $('.ispagination').empty();
                $('.ispagination').append(paginationHTML)
                window.paginationPageNumber = 1;
            }
            $('.ispagination').show();

            checkMappedUserDetails(mappedUserDetail, start, end, storeCount);

        }).catch(error => {
            console.error('getMap API::error: ' + error)
            displayStatus('danger', 'Cannot get User Mapping Details')
        })
}

function selectPageNo() {
    getMappingDetails(parseInt(document.getElementById("selectPageNo").value));
    window.paginationPageNumber = parseInt(document.getElementById("selectPageNo").value);
}

function checkMappedUserDetails(mappedUserDetail, start, end, storeCount) {
    $('#spinner').css("visibility", "hidden");
    let mappedUserHtml = ``;
    if (mappedUserDetail.length != 0) {
        if (storeCount) {
            getpaginationagents(mappedUserDetail.slice(start, end));
        }
        else {
            getpaginationagents(mappedUserDetail);
        }
    }
    else if (mappedUserDetail.length == 0) {
        mappedUserHtml = `<tr><td colspan="5"><p style="text-align: center;font-size:15px">No data is available </p></td></tr>`;
        $('.maplistpage').show();
        $('#mappedUsers').empty()
            .append(mappedUserHtml);
        $('#mappedUsers').show()
        displayStatus('danger', 'No data is available')
    }
}

function showErrorMessage(message, tag) {
    $(`#${tag} p`).html(message);
    $(`#${tag}`).show();
    setTimeout(function () {
        $(`#${tag}`).hide();
    }, 3000);
}

$("#mytable1 tbody").on('click', '.editmap', function (e) {
    console.info('Mapping::editMap::e : ' + JSON.stringify(e))
    let editagentId = $(this).attr("data-agentId");
    var headers = {
        "Content-Type": "application/json"
    };

    var options = {
        headers: headers,
    };
    let url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping?agentId=${editagentId}`
    client.request.get(url, options)
        .then((data) => {
            let current_user = JSON.parse(data.response);
            console.info(`Mapping::EditMap::current_user : ${current_user}`)
            console.info(`Mapping::EditMap::current_user : ${current_user.count}`)
            if (current_user.count !== 0) {
                editMappingForm(current_user);
            }
            return false;
        }, function (error) {
            console.error('editUserMapInfo API::error: ' + error)
            displayStatus('danger', 'Cannot open mapping Information.')
        });
})

function editMappingForm(current_user) {
    console.info('Mapping::editMappingForm::current_user : ', current_user)
    $('#editMapping').show();
    $(".ispagination").hide();
    $('#newMappingForm').hide();
    $('#syncBtn').hide()
    $('#uploadBtn').hide()
    $('#chooseFilterLabel').hide()
    $('#chooseFilter').hide()
    $('#search').hide()
    $('#searchBtn').hide()
    $('.maplistpage').hide();
    $('#saveMapping').hide();
    $('.userMapPage').hide();
    $('#editMappingForm').show();
    $('#editMappingForm').empty();

    let dropdown = ``;
    for (let index = 0; index < exophoneNum.length; index++) {
        dropdown += `<fw-select-option value="${exophoneNum[index]}">${exophoneNum[index]}</fw-select-option>`;
    }
    var editAgentHtml = ``;
    editAgentHtml = `<div class="container_1">
                    <div class="row">
                    <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card card-signin my-5">
                    <div class="card-body">
                    <div class="successmessage" id="editsuccessmessage" style="display: none;">
                    <p></p>
                    </div>
                    <div class="errormessage" id="editerrormessage" style="display: none;">
                    <p></p>
                    </div>
                    <h2 class="card-title text-center">Edit Mapping</h2>
                    <div class="form-label-group">
                    <label for="inputEmail" class="formlabell">Caller Id<span style="color:red">*</span></label>`
    editAgentHtml += (current_user[0].exotelVirutalnumber && current_user[0].uploadStatus != 'failure') ?
        `<fw-select id="edit_exotelVirutalnumber" required="true" value="${current_user[0].exotelVirutalnumber}" placeholder="" icon-left=""state-text="">        
        ${dropdown}
        </fw-select>`:
        `<fw-select id="edit_exotelVirutalnumber" required="true" value="1" placeholder="" icon-left="" state-text="" >
        ${dropdown}
        </fw-select>`;
    editAgentHtml += `</div>
                    <div class="form-label-group">
                    <label for="inputPassword" class="formlabell">Mobile Number<span style="color:red">*</span></label>`
    editAgentHtml += (current_user[0].exotelNumber && current_user[0].uploadStatus != 'failure') ?
        `<fw-input class="" id="edit_exotelNumber" label="" state-text="" value="${current_user[0].exotelNumber}"
    state="" placeholder="" required ></fw-input>`:
        `<fw-input class="" id="edit_exotelNumber" label="" state-text="" value="${''}"
    state="" placeholder="" required ></fw-input>`;
    editAgentHtml +=
        `</div>
                    <div class="form-label-group">
                    <label for="edit_freshdeskAgentName" class="formlabell" style="padding-bottom: 0px;">Freshworks User<span style="color:red">*</span></label>                    
                    <fw-input type="hidden" id="edit_freshdeskAgentId" label="" state-text="" value="${current_user[0].agentId}"
                    state="" placeholder="" disabled ></fw-input>
                    <fw-input class="" id="edit_freshdeskAgentName" label="" state-text="" value="${current_user[0].agentName}"
                    state="" placeholder="" disabled ></fw-input>
                    </div>
                    <div>
                    <label for="enableOutboundEdit" class="formlabell">Enable Outbound</label>  
                    `
    editAgentHtml += current_user[0].isOutboundEnable == 0 ?
        `
                                    <input type="checkbox" id="enableOutboundEdit" name="enableOutboundEdit">
                                    ` :
        `
                                    <input type="checkbox" id="enableOutboundEdit" name="enableOutboundEdit" checked>
                                    `
    editAgentHtml += `</div><br><br>
                    <button class="btn btn-lg btn-block text-uppercase"  onClick="editinguser(${current_user[0].agentId})" id="editMapping">Save</button>
                    <button type="button" class="btn btn-outline-dark editbackArrow" onClick="editbackAction()" id="edit_backarrow">Back</button>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>`;
    $('#editMappingForm').append(editAgentHtml);
}

function displayStatus(type, message) {
    client.interface.trigger('showNotify', {
        type: type,
        message: message
    });
}