function addCallDirectionImage(callDirection, callState, contactName, callFrom, callTo) {
    let callDirectionHtml = '';
    if (callDirection == "incoming" && (callState == "completed" || callState == "free")) {
        callDirectionHtml += `<div class="icon-sec bg-light-green text-center">
                    <svg width="20" height="20" class="incoming" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
                    <style type="text/css">
                    .incoming .st0{fill:#00A886;}
                    </style>
                    <path class="st0" d="M4.3,27H19c0.7,0,1.3-0.6,1.3-1.3c0-0.7-0.6-1.3-1.3-1.3H7.6L26.6,5.3c0.5-0.5,0.5-1.4,0-1.9  C26.3,3.1,26,3,25.7,3c-0.3,0-0.7,0.1-0.9,0.4L5.7,22.4V11c0-0.7-0.6-1.3-1.3-1.3C3.6,9.7,3,10.3,3,11v14.7C3,26.4,3.6,27,4.3,27z"/>
                    </svg></div></div>
                    <div class="col-5 cls_call_1">
                    <div class="name">
                    <p class="cls_call_p">${contactName}</p>
                    <span class="w100">${callFrom}</span>
                    </div>
                    </div>`;
    }
    else if ((callDirection == "incoming") && (callState == "canceled" || callState == "no-answer" || callState == "busy" || callState == "failed")) {
        callDirectionHtml += `<div class="icon-sec bg-light-pink bg-light-blue text-center">
                                <svg class="missed" width="20" height="20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
                                <style type="text/css">
                                .missed .st0{fill:#E43538;stroke:#E43538;stroke-miterlimit:10;}
                                </style>
                                <path class="st0" d="M28.4,11.8c-0.3-0.4-0.9-0.4-1.2-0.1l-9.9,8c-1.4,1.1-3.6,0.9-4.8-0.5l-7-8.7l7.6,0.8c0.5,0.1,0.9-0.3,1-0.8  c0.1-0.5-0.3-0.9-0.8-1L3.5,8.4C3,8.3,2.6,8.7,2.5,9.1l-1.1,9.7c-0.1,0.5,0.3,0.9,0.8,1c0.5,0.1,0.9-0.3,1-0.8l0.8-7.6l7,8.7  c0.9,1.1,2.1,1.7,3.5,1.9c1.4,0.2,2.7-0.2,3.8-1.1l9.9-8C28.6,12.7,28.7,12.2,28.4,11.8z"/>
                                </svg></div></div>
                                <div class="col-5 cls_call_1">
                                <div class="name">
                                <p class="cls_call_p">${contactName}</p>
                                <span class="w100">${callFrom}</span>
                                </div>
                                </div>`;
    }
    else if ((callDirection == "outbound-api")) {  //direction = 'outbound-dial' or 'outbound-api'
        callDirectionHtml += `<div class="icon-sec bg-light-blue text-center">
                                <svg class="outgoing" width="20" height="20" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
                                <style type="text/css">
                                .outgoing .st0{fill:#2c5cc5;}
                                </style>
                                <path class="st0" d="M25.7,3H11c-0.7,0-1.3,0.6-1.3,1.3s0.6,1.3,1.3,1.3h11.4L3.4,24.7c-0.5,0.5-0.5,1.4,0,1.9C3.7,26.9,4,27,4.3,27  c0.3,0,0.7-0.1,0.9-0.4L24.3,7.6V19c0,0.7,0.6,1.3,1.3,1.3c0.7,0,1.3-0.6,1.3-1.3V4.3C27,3.6,26.4,3,25.7,3z"/>
                                </svg></div></div>
                                <div class="col-5 cls_call_1">
                                <div class="name">
                                <p class="cls_call_p">${contactName}<p>
                                <span class="w100">${callTo}</</span>
                                </div>
                                </div>`
    }
    return callDirectionHtml;
}

function callInformation(response) {
    let callInfoHTML = '';
    for (let res of response) {
        console.info('callInfo::callInformation:callDirection : ', res.callDirection)
        console.info('callInfo::callInformation:callLogId : ', res.callLogId)
        let number = res.callDirection === 'incoming' ? res.callFrom : res.callTo;
        let contactName = res.contactName == null ? 'Unknown' : res.contactName;
        let duration = res.duration == null ? 0 : res.duration;
        let durationFormat = formatCallDuration(duration);
        if (res.callLogId) {
            callInfoHTML +=
                `<div class="list-sec pt-xl-3 pl-xl-3 pr-xl-3 pb-xl-3 row align-items-center cls_call_B">
                    <div class="row align-items-center">
                    <div class="col-2 pdrgt0">`;
            callInfoHTML += addCallDirectionImage(res.callDirection, res.callState, contactName, res.callFrom, res.callTo);
            callInfoHTML += `<div class="col-5 text-right pdlft0">
                        <div class="time">
                        <span class="seconds w100 cls_call_span">${durationFormat}</span>
                        <span class="hours w100" style="font-size:12px !important">${res.startTime}</span>
                        </div>
                        <div class="call-over-action">
                        <button class="btn btn-round-white" onClick="getTickets('${res.callFrom}','${res.callTo}', '${res.callSid}', '${res.callDirection}', '${res.callState}')">
                        <span style="font-size: 11px;">Add Note<span>
                        </button>
                        <button class="btn btn-round-green" data-toggle="tooltip" title="make call" onClick="makeOutboundCall('${number}')">
                        <i class="fas fa-phone-alt" style="font-size: 15px;color: #20c63c;"></i>
                        </button>
                        </div>
                        </div>
                        </div>
                        </div>`;
        }
    }
    return callInfoHTML;
}

function formatCallDuration(duration) {
    let hour = Math.floor(parseInt(duration) / 60 / 60) > 9 ? "" + Math.floor(parseInt(duration) / 60 / 60) : '0' + Math.floor(parseInt(duration) / 60 / 60)
    let minute = Math.floor(parseInt(duration) % 3600 / 60) > 9 ? "" + Math.floor(parseInt(duration) % 3600 / 60) : '0' + Math.floor(parseInt(duration) % 3600 / 60)
    let second = parseInt(duration) % 3600 % 60 > 9 ? "" + parseInt(duration) % 3600 % 60 : '0' + parseInt(duration) % 3600 % 60
    return hour + ':' + minute + ':' + second;
}