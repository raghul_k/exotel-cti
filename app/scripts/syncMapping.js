function syncUserMapping() {
    postAgentList();
    hideModal();
    $('#fieldMapping').empty();
    $('#fieldMapping').addClass('successmessage')
    $('#fieldMapping').html('\tIt will take some time to sync all the users.<br>Please revisit this section to check the sync status or verify it from the User Mapping section.');
    $('#fieldMapping').show();
}

function postAgentList() {

    const headers = {
        "Content-Type": "application/json"
    };
    const options = {
        headers: headers,

    };
    let url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping/sync`
    client.request.get(url, options)
        .then((data) => {
            console.info('SyncMapping::postAgentList::/usermapping/bulk API response : ' + data)
            $('#fieldMapping').hide();
            getMappingDetails()

        }).catch(error => {
            console.error('SyncMapping::postAgentList::/usermapping/bulk API error: ' + JSON.stringify(error, null, 2))
            displayStatus('danger', 'Please Contact Support.')
        })
}

function hideModal() {
    document.getElementById('verifyPopup').visible = false;
}