let footerHTML = ` 
 <div class="dial-footer">
 <div class="col-12 footer-menu" style="margin: 0px 0px 0px -10px;">
 <div class="row justify-content-center ">
 <div class="nav align-items-center">
 <ul> 
 <li class="nav-item ">
 <a class="nav-link" href="#" data-toggle="tooltip" title="Call History" onClick="footerClick()">   
 <svg class="clock" width="25" height="25" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 30 30" style="enable-background:new 0 0 30 30;" xml:space="preserve">
 <style type="text/css">
 .clock .st0{fill:none;stroke:#92a2b1;stroke-width:2;stroke-linecap:round;stroke-miterlimit:10;}
 .clock .st1{fill:none;stroke:#92a2b1;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
 </style>
 <polyline class="st0" points="14.7,10.2 14.7,15.7 20.2,15.7 "/>
 <polyline class="st1" points="8.4,11.3 8.4,4.8 1.8,4.8 "/>
 <path class="st1" d="M24.5,8.4c-0.8-1.2-1.7-2.3-2.9-3.2"/>
 <path class="st1" d="M27.4,16.6c0.2-1.6,0.1-3.2-0.3-4.7"/>
 <path class="st1" d="M7.1,6.2C1,10.7,1,19.9,6,24.8c6.1,6,17.2,3.2,20.5-4.4"/>
 <path class="st1" d="M17.5,2.9c-1.4-0.3-2.9-0.3-4.3-0.1"/>
 </svg>
 </a>
 </li>  
 </ul> 
 </div>
 </div>
 </div> 
 </div>`

function footerClick() {
    if ($('.callerDetailsPage').is(':visible') && ($("#description").val() != '')) {
        client.interface.trigger("showConfirm", {
            title: "Warning!",
            message: "Do you want to redirect the page without saving the changes?", saveLabel: "Yes", cancelLabel: "No"
        }).then(function (result) {
            console.info('Inboundpopup::footerClick:result : ' + JSON.stringify(result))
            if (result.success.string == "Interface action triggered successfully") {
                callHistory();
            }
        }).catch(function (error) {
            if (error) console.error('error = ', error)
        });
    }
    else {
        callHistory();
    }
}

function incomingScreenPop(callDetail, apiValue, contactNumber) {
    console.info(`callDetailcallDetail::${callDetail}`)
    console.info(`callDetailcallDetail::${callDetail.Status}`)
    console.info(`apivalue::${apiValue}`)
    let callDataResponse = callDetail;
    var callStatus = callDataResponse.Status;
    var callednum = contactNumber;

    if (apiValue == 'Dial' && callStatus == 'busy') {
        $('.recentTickets').show();
        $(".Reply").hide();
        $(".ModalPopup").hide();
        $(".showContacts").hide();
        $('#callHistoryList').hide();
        fetch_contact(callednum, callDataResponse);
    }

    else if (apiValue == 'Terminal' || apiValue == 'missed') {
        console.info('Inboundpopup::incomingScreenPop:callerDetailsPage : ', $('.callerDetailsPage').is(':visible'))
        console.info('Inboundpopup::incomingScreenPop:callHistoryList : ', $('.callHistoryList').is(':visible'))
        console.info('Inboundpopup::incomingScreenPop:callerDetails : ', $('.callerDetails').is(':visible'))
        if ($('.callerDetails').is(':visible')) {
            setTimeout(() => {
                const arrData = [];
                arrData.push('allCalls');
                callHistory(arrData);
            }, 5000);
        }
        displayStatus('success', 'Call Ended')
    }
}


function fetch_contact(phone, calldata) {
    var callDetails = {};
    var url_contact = Clienturl + "/api/search?q=" + encodeURIComponent(phone) + "&include=contact";
    var url_lead = Clienturl + "/api/search?q=" + encodeURIComponent(phone) + "&include=lead";

    console.log("contact_url" + url_contact, "lead_url" + url_lead);
    var headers = {
        "Authorization": 'Token token=' + fw_apikey,
        "Content-Type": "application/json"
    }

    client.request.get(url_contact, { headers })
        .then(function (contact) {
            console.log("Contact--------->" + contact);
            callDetails.contact = jQuery.parseJSON(contact.response);
            callDetails.company = '';
            if (callDetails.contact.length != 0) {
                callDetails.direction = 'incoming';
                callDetails.phone = phone;
                contactPage(callDetails, "alreadyexist", calldata);
            } else {

                if (Clientname == "freshsales") {
                    client.request.get(url_lead, { headers })
                        .then(function (leads) {
                            console.log("leads--------->" + leads);
                            callDetails.contact = jQuery.parseJSON(leads.response);
                            callDetails.company = '';
                            if (callDetails.contact.length != 0) {
                                callDetails.direction = 'incoming';
                                callDetails.phone = phone;
                                contactPage(callDetails, "alreadyexist", calldata);
                            }
                            else {
                                callDetails.phone = phone;
                                callDetails.name = 'Unknown';
                                callDetails.direction = 'incoming';
                                contactPage(callDetails, "newcontact", calldata);
                            }
                        },
                            function (error) {
                                console.error('Error in getting Leads error: ' + error);
                                displayStatus('danger', 'Cannot get the Leads. Please contact support for help');
                            });
                }
                else {
                    callDetails.phone = phone;
                    callDetails.name = 'Unknown';
                    callDetails.direction = 'incoming';
                    contactPage(callDetails, "newcontact", calldata);
                }
            }
        },
            function (error) {
                console.error('Error in getting Contact error: ' + error);
                displayStatus('danger', 'Cannot get the Contact. Please contact support for help');
            });
}

function contactPage(param1, param2, call) {
    var calldata = JSON.stringify(calldata);
    if (param2 == "alreadyexist") {
        console.info("Inboundpopup::contactPage::alreadyexisting Contact : ", param1.contact);
        var contactObj = param1.contact[0];
    }
    else if (param2 == "newcontact") {
        var contactObj = null;
    }

    $(".callerDetails").show();
    $(".callerDetails").empty();
    $("body.callerDetails").css("overflow", "hidden");
    let contact = '';

    if (contactObj == null) {
        contact = unknownScreenPop(param1.name, param1.phone, 'incoming', param1, call.CallSid);
    }
    else if (contactObj != null) {
        contact = knownScreenPop(contactObj.name, param1.phone, 'incoming', param1, call.CallSid);
    }

    contact += footerHTML;
    $(".callerDetails").append(contact);
}


function displayStatus(type, message) {
    client.interface.trigger('showNotify', {
        type: type,
        message: message
    });
}