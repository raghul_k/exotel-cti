function socketConnection() {
    var socket;
    console.info('SOCKET CONNECTION DONE NOW')
    let socketURL = Endpoint_url;

    console.info('socketURL = ', socketURL)
    socket = io(socketURL + '/');

    socket.on('connect', () => {
        console.info('Socket Connected');
        console.info('Room Id (agentId) : ', parseInt(loggedInId))
    });

    socket.on('outbound_call', function (data) {
        console.info('Main:App Initialized::Socket : outbound_call data : ', JSON.stringify(data))
        console.info('Main:App Initialized::Page Info : Call Note & Ticket history Page : ' + $('.callerDetailsPage').is(':visible') + ' Call History Page : ' + $('.callHistoryList').is(':visible') + ' Inbound Screen Pop : ' + $('.callerDetails').is(':visible'))
        if ($('.callerDetails').is(':visible')) {
            setTimeout(() => {
                const arrData = [];
                arrData.push('allCalls');
                callHistory(arrData);
                displayStatus('success', 'Call Ended')
            }, 5000);
        }
    })

    socket.on('incoming_call', function (data) {
        console.info('Main:App Initialized::Socket : incoming_call data : ' + JSON.stringify(data));
        let logId = parseInt(loggedInId);
        if (JSON.parse(JSON.parse(data.agentId)) === logId) {
            var contact = htmlEncode(data.callInfo.CallFrom);
            var contactdata = contact.substr(contact.length - 10);
            openCTIPanel(data.callInfo, data.callInfo.EventType, contactdata);
        }
    });
}

function htmlEncode(s) {
    var ntable = {
        "&": "amp",
        "<": "lt",
        ">": "gt",
        "\"": "quot"
    };
    s = s.replace(/[&<>"]/g, function (ch) {
        return "&" + ntable[ch] + ";";
    })
    s = s.replace(/[^ -\x7e]/g, function (ch) {
        return "&#" + ch.charCodeAt(0).toString() + ";";
    });
    return s;
}