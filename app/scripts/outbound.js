function makeOutboundCall(toNumber) {
    console.info('Outbound::makeOutboundCall:toNumber : ', toNumber)
    let logId = parseInt(loggedInId);
    var headers = {
        "Content-Type": "application/json"
    };
    var options = {
        headers: headers,
    };
    let url = `${Endpoint_url}/${Clientname}/${exotelSid}/userMapping?agentId=${logId}`;
    client.request.get(url, options)
        .then((mappedNumber) => {
            let mappedNumberResponse = JSON.parse(mappedNumber.response);
            console.info('Outbound::makeOutboundCall::getAgentNumberANDCallerIdById API::mappedNumber : ', mappedNumberResponse)
            let mappedPhoneNumber = (mappedNumberResponse.count != 0) ? mappedNumberResponse[0].exotelNumber : '';
            let callerId = (mappedNumberResponse.length != 0) ? mappedNumberResponse[0].exotelVirutalnumber : '';
            console.info('Outbound::makeOutboundCall::getAgentNumberANDCallerIdById API::mappedPhoneNumber & callerId = ', mappedPhoneNumber + ' & ' + callerId + 'exotelSid = ' + exotelSid)
            if (mappedNumberResponse[0].isOutboundEnable != 0) {
                client.interface.trigger("show", { id: "phoneApp" })
                    .then(function (openCTI) {
                        console.info('Outbound::makeOutboundCall::getAgentNumberANDCallerIdById API::openCTI : ' + openCTI)
                        let formData = {
                            'formData': {
                                "From": mappedPhoneNumber,
                                "To": toNumber,
                                "CallerId": callerId,
                                "StatusCallback": `${Endpoint_url}/${exotelSid}/outBoundCall`,
                                "StatusCallbackEvents[0]": "terminal",
                                "StatusCallbackContentType": "application/json"
                            }
                        }
                        client.request.invoke("OutboundCallMethod", formData).then(
                            function (outboundCallData) {
                                console.log(outboundCallData);

                                let outboundCallDataResponse = JSON.parse(outboundCallData.response);
                                console.info('Outbound::makeOutboundCall::OUTBOUND CALL API::response : ', outboundCallDataResponse.Call)
                                console.info('Outbound::makeOutboundCall::OUTBOUND CALL API::response : ', outboundCallDataResponse.Call.Sid)
                                let callDetails = {}
                                let url_contact = Clienturl + "/api/search?q=" + encodeURIComponent(toNumber) + "&include=contact";
                                var url_lead = Clienturl + "/api/search?q=" + encodeURIComponent(toNumber) + "&include=lead";
                                var headers = {
                                    "Authorization": 'Token token=' + fw_apikey,
                                    "Content-Type": "application/json"
                                }
                                client.request.get(url_contact, { headers })
                                    .then(function (contact) {
                                        console.info('Outbound::makeOutboundCall::Contact API::responde:', jQuery.parseJSON(contact.response))
                                        callDetails.contact = jQuery.parseJSON(contact.response);
                                        let pageRender = '';
                                        if (jQuery.parseJSON(contact.response).length != 0) {
                                            let contactResponse = jQuery.parseJSON(contact.response)[0];
                                            callDetails.name = contactResponse.name;
                                            callDetails.phone = toNumber;
                                            pageRender = knownScreenPop(callDetails.name, callDetails.phone, outboundCallDataResponse.Call.Direction, callDetails, outboundCallDataResponse.Call.Sid);
                                            screenPop(pageRender)
                                        }
                                        else {
                                            if (Clientname == "freshsales") {
                                                client.request.get(url_lead, { headers })
                                                    .then(function (leads) {
                                                        console.info('Outbound::makeOutboundCall::Contact API::responde:', jQuery.parseJSON(leads.response))
                                                        callDetails.contact = jQuery.parseJSON(leads.response);
                                                        let pageRender = '';
                                                        if (jQuery.parseJSON(leads.response).length != 0) {
                                                            let contactResponse = jQuery.parseJSON(leads.response)[0];
                                                            callDetails.name = contactResponse.name;
                                                            callDetails.phone = toNumber;
                                                            pageRender = knownScreenPop(callDetails.name, callDetails.phone, outboundCallDataResponse.Call.Direction, callDetails, outboundCallDataResponse.Call.Sid);
                                                            screenPop(pageRender)
                                                        }
                                                        else {
                                                            callDetails.name = 'Unknown'
                                                            pageRender = unknownScreenPop(callDetails.name, toNumber, outboundCallDataResponse.Call.Direction, callDetails, outboundCallDataResponse.Call.Sid);
                                                            screenPop(pageRender)
                                                        }
                                                    }, function (error) {
                                                        console.error('Outbound:makeOutboundCall:: GET Contact API error', error)
                                                        displayStatus('danger', 'Cannot get the Contact. Please contact support for help')
                                                    })
                                            }
                                            else {
                                                callDetails.name = 'Unknown'
                                                pageRender = unknownScreenPop(callDetails.name, toNumber, outboundCallDataResponse.Call.Direction, callDetails, outboundCallDataResponse.Call.Sid);
                                                screenPop(pageRender)
                                            }
                                        }
                                    }, function (error) {
                                        console.error('Outbound:makeOutboundCall:: GET Contact API error', error)
                                        displayStatus('danger', 'Cannot get the Contact. Please contact support for help')
                                    })
                            }).catch((error) => {
                                console.error('Cannot make Outgoing Call error = ', error)
                                displayStatus('danger', 'Cannot Make a Call. Please contact support for help')
                            })
                    }).catch(function (error) {
                        if (error) console.error('Outbound::Interface API show::openCTI error:', error)
                    });
            }
            else {
                displayStatus('danger', 'Not allowed to make outbound call');
            }
        }, function (error) {
            console.error('Outbound:makeOutboundCall:: /getAgentNumberANDCallerIdById API error ', error);
            displayStatus('danger', 'Agent does not have Mobile Number. Contact Admin for support')
        })
}