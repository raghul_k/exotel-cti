function HttpRequests(authInfo) {
    this.fw_domain = authInfo.fw_domain;
    this.fw_api_key = authInfo.fw_api_key;
    this.exotel_api_key = authInfo.exotel_api_key;
    this.exotel_api_token = authInfo.exotel_api_token;
    this.Exotel_sid = authInfo.Exotel_sid;
    this.Exotel_acc = authInfo.Exotel_acc == "Singapore" ? 'api.exotel.com' : 'api.in.exotel.com';
    this.fsEndpoints = {
        getLeadFields: "/api/settings/leads/fields",
    };
    this.getLeadFieldsEndPoint = "/api/settings/leads/fields";
}

HttpRequests.prototype.getLeadFields = function () {
    if (Clientname == "freshsales") {
        var url = "https://" + this.fw_domain + this.fsEndpoints.getLeadFields;
    } else {
        var url = "https://" + this.fw_domain + "/crm/sales" + this.fsEndpoints.getLeadFields;
    }
    console.log("url===========>", url, "this.domain=========>", this.fw_domain);
    var headers = {
        Authorization: "Token token=" + this.fw_api_key,
    };
    return client.request.get(url, { headers });
};

HttpRequests.prototype.getExotelExophone = function (data) {
    let url = "https://" + this.Exotel_acc + "/v2_beta/Accounts/" + this.Exotel_sid + "/IncomingPhoneNumbers"
    console.log("url==========>", url);
    let Exotel_headers = {
        Authorization: "Basic " + btoa(`${this.exotel_api_key}:${this.exotel_api_token}`),
        accept: "application/json",
    };
    console.log("Exotel_headers=======>", Exotel_headers);
    return client.request.get(url, { headers: Exotel_headers });
};

HttpRequests.prototype.getOwnerdetails = function () {
    if (Clientname == "freshsales") {
        var url = "https://" + this.fw_domain + "/api/selector/owners";
    } else {
        var url = "https://" + this.fw_domain + "/crm/sales/api/selector/owners";
    }
    console.log("url===========>", url, "this.domain=========>", this.fw_domain);
    var headers = {
        Authorization: "Token token=" + this.fw_api_key,
    };
    return client.request.get(url, { headers });
};

$("#Exotel_acc").on('change', function () {
    var acc = $("#Exotel_acc").val();
    console.log("test", acc);
    var tip = "";
    if (acc == "Singapore") {
        tip = "http://my.exotel.com/auth/login?redirect=apisettings/site#api-credentials"
        console.log("Singapore", tip);
    } else {
        tip = "http://my.in.exotel.com/auth/login?redirect=apisettings/site#api-credentials"
        console.log("Mumbai", tip);
    }
});

//HELPER FUNCTIONS

function notify(message, type = "success") {
    var bgColor = type == "success" ? "#02b875" : "#DC161F";
    Toastify({
        text: message,
        duration: 2000,
        newWindow: true,
        close: true,
        gravity: "top",
        backgroundColor: bgColor,
        position: "right",
        stopOnFocus: true,
    }).showToast();
}

function handleError(err) {
    console.log(err);
    if (err.status == 400) {
        notify(err.response.concat(" for Freshworks"), "error");
        return;
    }
    if (err.status == 500) {
        notify("Freshworks Domain Invaild", "error");
        return;

    }
    if (err.status == 403) {
        notify("Exotel details are Missing or Invalid", "error");
        return;
    }
    if (err.status == 401) {
        var errObj = err.response;
        if (errObj == "Authentication is required to view this page.") {
            notify("Exotel details are Missing or Invalid", "error");
            return;
        };
        if (errObj == '{"RestException":{"Status":401,"Message":"Unauthorized; ","Code":34010}}') {
            notify("Exotel details are Missing or Invalid", "error");
            return;
        }
        notify("Freshworks details are Missing or Invalid", "error");
        return;
    }

    var errObj = JSON.parse(err.response);
    if (errObj.hasOwnProperty("login")) {
        notify(errObj.message.concat("for Freshworks"), "error");
        return;
    };
    notify("Enter Valid details", "error");
};

let changeEvent = () => {
    var Data = $('#Exotel_acc').val();
    var link = "";
    if (Data == "Singapore") {
        link = "http://my.exotel.com/auth/login?redirect=apisettings/site#api-credentials"
    } else {
        link = "http://my.in.exotel.com/auth/login?redirect=apisettings/site#api-credentials"
    }
    var setId = document.getElementsByClassName(`cls_clkhere`);
    setId.forEach(setId => {
        setId.setAttribute("href", link);
    });

};

let appendOwnerslist = (owners) => {
    var ownerdtls = owners.users;
    var catOptions = "";
    for (let i = 0; i < ownerdtls.length; i++) {
        catOptions += "<option value=" + ownerdtls[i].id + ">" + ownerdtls[i].display_name + " (" + ownerdtls[i].id + ")" + "</option>";
    }
    $("#lstFruits").append(catOptions);
};

var expanded = false;

function showCheckboxes() {
    var checkboxes = document.getElementById("checkboxes");
    if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
    } else {
        checkboxes.style.display = "none";
        checkboxes.style.overflow = "auto";
        expanded = false;
    }
};

// function checkOptions() {
//     els = document.getElementsByName('categories');
//     var selectedChecks = "", qtChecks = 0;
//     for (i = 0; i < els.length; i++) {
//         if (els[i].checked) {
//             if (qtChecks > 0) selectedChecks += ", "
//             selectedChecks += els[i].value;
//             qtChecks++;
//         }
//     }
//     if (selectedChecks != "") {
//         document.getElementById("defaultCategory").innerText = selectedChecks;
//     } else {
//         document.getElementById("defaultCategory").innerText = "Select an option";
//     }
// }